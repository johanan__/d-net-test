﻿import React from "react";
import { Link } from "react-router-dom";
import styles from "./navbar.module.css";

//TODO Web Template Studio: Add a new link in the NavBar for your page here.
// A skip link is included as an accessibility best practice. For more information visit https://www.w3.org/WAI/WCAG21/Techniques/general/G1.
const NavBar = (props) => {
  let Navigation = () => {
    if(props.role == "index")
    {
      return (
        <div className="navbar-nav">
          <Link className="nav-item nav-link active" to="/login">
            Login
          </Link>
          <Link className="nav-item nav-link active" to="/register">
            Register
          </Link>
        </div>
      );
    }
    else if(props.role == "admin")
    {
      return (
        <div className="navbar-nav">
          <Link className="nav-item nav-link active" to="/admin/addItem">
            Add Item
          </Link>
          <Link className="nav-item nav-link active" to="/admin/users">
            Manage User
          </Link>
          <Link className="nav-item nav-link active" to="/logout">
            Logout
          </Link>
        </div>
      );
    }
    else
    {
      return (
        <div className="navbar-nav">
          <Link className="nav-item nav-link active" to="/editItem">
            Edit Profile
          </Link>
          <Link className="nav-item nav-link active" to="/items">
            Items
          </Link>
          <Link className="nav-item nav-link active" to="/logout">
            Logout
          </Link>
        </div>
      );
    }
  }
  return (
    <React.Fragment>
      <div className={styles.skipLink}>
        <a href="#mainContent">Skip to Main Content</a>
      </div>
      <nav className="navbar navbar-expand-sm navbar-light border-bottom justify-content-between">
        <Link className="navbar-brand" to="/">
          aD NET
        </Link>
        <Navigation></Navigation>
      </nav>
    </React.Fragment>
  );
}
export default NavBar;
