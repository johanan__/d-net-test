﻿import React, { Fragment } from "react";
import { Link } from "react-router-dom";
import Card from "./card";
const Home = () => {
  return (
    <Fragment>
          <nav className="navbar navbar-expand-sm navbar-light border-bottom justify-content-between">
      <Link className="navbar-brand" to="/">
        aD NET
      </Link>
      <div className="navbar-nav">
          <Link className="nav-item nav-link active" to="/editProfile">
            Edit Profile
          </Link>
          <Link className="nav-item nav-link active" to="/items">
            Items
          </Link>
          <Link className="nav-item nav-link active" to="/login">
            Logout
          </Link>
        </div>
    </nav>
    <main id="mainContent">
    <div className="container">
      <div className="row mt-5 p-0">
        <h3>Offers</h3>
        <div className="col-md-3">
          <Card
            judul="paket 15k"
            harga="20000"
          ></Card>
        </div>
        <div className="col-md-3">
          <Card
            judul="paket 15k"
            harga="20000"
          ></Card>
        </div>


      </div>
    </div>
  </main>
    </Fragment>
  );
}
export default Home;
