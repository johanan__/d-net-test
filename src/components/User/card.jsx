import React, { Component } from 'react';

const Card = (props) => {
    return (
        <div class="card" style={{border: "0.5px solid #f2f3fc"}}>
            <div class="bg-card p-5">
                <i class="fa fa-rocket" aria-hidden="true"></i>
    <p class="sub-icon font-weight-bold">{props.judul}</p>
            </div>
            <div class="card-body">
            <div class="row">
                <div class="col-md-12 p-5 theme-font">
    <span style={{fontSize: "70px;"}}>Rp. {props.harga}</span>
                </div>
            </div>
            <div class="d-flex justify-content-center text-center mt-5">
                <div class="module-border-wrap">
                    <div class="module font-weight-bold">
                        Buy
                    </div>
                </div>
            </div>
            </div>
        </div>
    );
}

export default Card;