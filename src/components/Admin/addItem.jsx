import React, { useState } from "react";

import { ADD_ITEM, ITEMS } from "../queries/queries";
import { useMutation } from "@apollo/client";
import ListItem from "./listItem";
const AddItem = () => {
    const [nama,setNama] = useState("item");
    const [harga,setHarga] = useState(1);
    const [stock,setStock] = useState(1);
    const [flash,setFlash] = useState("");
    const [addItem, { data, error }] = useMutation(ADD_ITEM, {
        onCompleted(data){
            let alert = (
                <div className="alert alert-success" role="alert">
                    Add item Berhasil!
                </div>
            );
            setFlash(alert);
        },
        onError(err){
            console.log(err.graphQLErrors);
            let alert = (
                <div className="alert alert-success" role="alert">
                    {err.graphQLErrors}
                </div>
            );
            setFlash(alert);
        }
    });
    let handleSubmit = async (e) => {
        e.preventDefault();
        try {
            await addItem({ variables: 
                {
                    params:{
                        "nama_item":nama,
                        "harga":parseInt(harga),
                        "stock":parseInt(stock)
                    }
                },
                refetchQueries: [{query:ITEMS}]
            });
        } catch (error) {
            console.log(error);
        }

    }
  return (
      <div className="row">
          <div className="col-md-12">
            <h1>Add Item</h1>
                <form className="my-5">
                    <div className="form-group">
                        <label htmlFor="nama">Item Name</label>
                        <input type="text" className="form-control" id="email" placeholder="Name" onChange={(e) => setNama(e.target.value)} />
                    </div>
                    <div className="form-group">
                        <label htmlFor="harga">Item Price</label>
                        <input type="number" className="form-control" id="harga" placeholder="Price" onChange={(e) => setHarga(e.target.value)} />
                    </div>
                    <div className="form-group">
                        <label htmlFor="stock">Stock</label>
                        <input type="number" className="form-control" id="stock" placeholder="Stock" onChange={(e) => setStock(e.target.value)} />
                    </div>
                    <button type="submit" className="btn btn-primary" onClick={handleSubmit}>Submit</button>
                </form>
                {flash}
                <ListItem></ListItem>
          </div>
      </div>
  );
}
export default AddItem;
