import React from "react";

import { ITEMS } from '../queries/queries';
import { useQuery } from "@apollo/client";
const ListItem = () => {
    const { loading, error, data } = useQuery(ITEMS); 
    if (loading) return <p>Loading...</p>;
    if (error) return <p>Error :(</p>;
    return (
        <div className="row">
            <div className="col-md-12">
                <h1>your Items</h1>
                <table className="table">
                    <thead>
                        <tr>
                            <td>id_item</td>
                            <td>nama_item</td>
                            <td>harga</td>
                            <td>stock</td>
                            <td>status</td>
                        </tr>
                    </thead>
                    <tbody>
                    {
                        data.items.map(({id_item,
                            nama_item,
                            harga,
                            stock,
                            status}) => {
                            return <tr key={id_item}>
                                <td>{id_item}</td>
                                <td>{nama_item}</td>
                                <td>{harga}</td>
                                <td>{stock}</td>
                                <td>{status}</td>
                            </tr>
                        })
                    }
                    </tbody>
                </table>
            </div>
        </div>
    );
}
export default ListItem;
