import React, { Fragment } from "react";
import { Switch, Route, Link } from "react-router-dom";
import AddItem from "./addItem";
import ManageUser from "./manageUser";

const Admin = () => {
  return (
    <Fragment>
    <nav className="navbar navbar-expand-sm navbar-light border-bottom justify-content-between">
      <div className="navbar-nav">
        <Link className="nav-item nav-link active" to="/admin/addItem">
          Add Item
        </Link>
        <Link className="nav-item nav-link active" to="/admin/users">
          Manage User
        </Link>
        <Link className="nav-item nav-link active" to="/login">
          Logout
        </Link>
      </div>
    </nav>
  <main id="mainContent">
      <div className="container">
        <div className="row mt-5 p-0">
          <div className="col-md-12">
            <h3>Welcome, Admin</h3>
          </div>
        </div>
        <div className="row">
          <div className="col-md-12">
            <Switch>
              <Route path = "/admin/addItem" component = { AddItem } />
              <Route path = "/admin/users" component = { ManageUser } />
            </Switch>
          </div>
        </div>
      </div>
    </main>
    </Fragment>
    

  );
}
export default Admin;
