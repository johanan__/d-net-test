import React from "react";

import { USER } from '../queries/queries';
import { useQuery } from "@apollo/client";
const ManageUser = () => {
    const { loading, error, data } = useQuery(USER); 
    if (loading) return <p>Loading...</p>;
    if (error) return <p>Error :(</p>;
    return (
        <div className="row">
            <div className="col-md-12">
                <h1>Manage User</h1>
                <table className="table">
                    <thead>
                        <tr>
                            <td>id_user</td>
                            <td>nama</td>
                            <td>email</td>
                            <td>no_telp</td>
                            <td>alamat</td>
                            <td>tgl_lahir</td>
                            <td>saldo</td>
                        </tr>
                    </thead>
                    {
                        data.users.map(({id_user, nama, email, no_telp, alamat, tgl_lahir, saldo}) => {
                            return <tr>
                                <td>{id_user}</td>
                                <td>{nama}</td>
                                <td>{email}</td>
                                <td>{no_telp}</td>
                                <td>{alamat}</td>
                                <td>{tgl_lahir}</td>
                                <td>{saldo}</td>
                            </tr>
                        })
                    }
                </table>
            </div>
        </div>
    );
}
export default ManageUser;
