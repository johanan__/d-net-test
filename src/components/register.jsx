import React, { Fragment, useState } from "react";
import { useMutation } from '@apollo/client';

import { REGISTER } from "./queries/queries";
import { Link } from "react-router-dom";
const Register = () => {
    const [email, setEmail] = useState(null);
    const [password, setPassword] = useState(null);
    const [cpassword, setCpassword] = useState(null);
    const [nama, setNama] = useState(null);
    const [no_telp, setNo_telp] = useState(null);
    const [alamat, setAlamat] = useState(null);
    const [tgl_lahir, setTgl_lahir] = useState(null);
    const [flash,setFlash] = useState("");
    const [registerUser, { data, error }] = useMutation(REGISTER, {
        onError(err){
            console.log(err.graphQLErrors);
        }
    });

    const handleSubmit = async (e) => {
        e.preventDefault();
        let alert = "";
        if (password === cpassword && password !== null) 
        {
            try {
                await registerUser({ variables: 
                    {
                        params:{
                            nama:nama,
                            email:email,
                            password:password,
                            no_telp:no_telp,
                            alamat:alamat,
                            tgl_lahir:tgl_lahir,
                        }
                    }
                });
                alert = (
                    <div className="alert alert-success" role="alert">
                        Register Berhasil!
                    </div>
                );
            } catch (error) {
                console.log(error);
                setFlash(error + "");
            }
        }
        else
        {
            alert = (
                <div className="alert alert-danger" role="alert">
                    password invalid!
                </div>
            );
        }
        setFlash(alert);
    };

    return (
        <Fragment>
        <nav className="navbar navbar-expand-sm navbar-light border-bottom justify-content-between">
        <Link className="navbar-brand" to="/">
        aD NET
      </Link>
          <div className="navbar-nav">
            <Link className="nav-item nav-link active" to="/login">
              Login
            </Link>
            <Link className="nav-item nav-link active" to="/register">
              Register
            </Link>
          </div>
        </nav>
        <div className="container">
            <div className="row">
                <div className="col-md-12">
                <h1>Register</h1>
            <form className="my-5">
                <div className="form-group">
                    <label htmlFor="email">Email address</label>
                    <input type="email" className="form-control" id="email" placeholder="Enter email" onChange={(e) => setEmail(e.target.value)} />
                </div>
                <div className="form-group">
                    <label htmlFor="password">Password</label>
                    <input type="password" className="form-control" id="password" placeholder="Enter password" onChange={(e) => setPassword(e.target.value)} />
                </div>
                <div className="form-group">
                    <label htmlFor="cpassword">Confirm Password</label>
                    <input type="password" className="form-control" id="cpassword" placeholder="Confirm password" onChange={(e) => setCpassword(e.target.value)} />
                </div>
                <div className="form-group">
                    <label htmlFor="nama">Your Name</label>
                    <input type="text" className="form-control" id="nama" placeholder="Your full name" onChange={(e) => setNama(e.target.value)} />
                </div>
                <div className="form-group">
                    <label htmlFor="no_hp">Phone Number</label>
                    <input type="text" className="form-control" id="no_hp" placeholder="Active phone number" onChange={(e) => setNo_telp(e.target.value)} />
                </div>
                <div className="form-group">
                    <label htmlFor="alamat">Home Address</label>
                    <input type="tel" className="form-control" id="alamat" placeholder="Home address" onChange={(e) => setAlamat(e.target.value)} />
                </div>
                <div className="form-group">
                    <label htmlFor="tgl_lahir">Birth Date</label>
                    <input type="date" className="form-control" id="tgl_lahir" placeholder="Your birth date" onChange={(e) => setTgl_lahir(e.target.value)} />
                </div>
                <button className="btn btn-primary" onClick={handleSubmit}>Submit</button>
                {flash}
            </form>
                </div>
            </div>
        </div>
            
        </Fragment>
    );
}
export default Register;
