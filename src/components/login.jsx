import React, { Fragment, useState } from "react";
import { useMutation } from '@apollo/client';

import { LOGIN } from "./queries/queries";
import { useHistory, Link } from "react-router-dom";
const Login = (props) => {
    const history = useHistory();
    const [email, setEmail] = useState(null);
    const [password, setPassword] = useState(null);
    const [flash,setFlash] = useState("");
    const [loginQuery, { data, error }] = useMutation(LOGIN, {
        onCompleted(data){
            console.log(data.login);
            if(data.login == "admin")
            {
                props.setRole("admin");
                history.push("/admin");
            }
            else if(data.login == "user")
            {
                props.setRole("user");
                history.push("/home");
            }
            else
            {
                let alert = (
                    <div className="alert alert-danger" role="alert">
                        password or email invalid!
                    </div>
                );
                setFlash(alert);
            }
        },
        onError(err){
            console.log(err.graphQLErrors);
            let alert = (
                <div className="alert alert-danger" role="alert">
                    password or email invalid!
                </div>
            );
            setFlash(alert);
        }
    });
    
    const handleSubmit = async (e) => {
        e.preventDefault();
        let alert = "";
        await loginQuery({variables:{
            email:email,
            password:password
        }});
    };
    return (
        <Fragment>
                    <nav className="navbar navbar-expand-sm navbar-light border-bottom justify-content-between">
                    <Link className="navbar-brand" to="/">
        aD NET
      </Link>
          <div className="navbar-nav">
            <Link className="nav-item nav-link active" to="/login">
              Login
            </Link>
            <Link className="nav-item nav-link active" to="/register">
              Register
            </Link>
          </div>
        </nav>
        <div className="container">
            <div className="row">
                <div className="col-md-12">
                <h1>Login</h1>
            <form className="my-5">
                <div className="form-group">
                    <label htmlFor="email">Email address</label>
                    <input type="email" className="form-control" id="email" placeholder="Enter email" onChange={(e) => setEmail(e.target.value)} />
                </div>
                <div className="form-group">
                    <label htmlFor="password">Password</label>
                    <input type="password" className="form-control" id="password" placeholder="Enter password" onChange={(e) => setPassword(e.target.value)} />
                </div>
                <button type="submit" className="btn btn-primary" onClick={handleSubmit}>Submit</button>
            </form>
            {flash}
                </div>
            </div>
        </div>
            
        </Fragment>
    );
}
export default Login;
