import { gql } from '@apollo/client';

const USER = gql`
{
	users{
    id_user
    nama
    email
    no_telp
    alamat
    tgl_lahir
    saldo
  }
}
`;

const REGISTER = gql`
    mutation($params: UserInput!) {
    createUser(input: $params) {
        nama
        email
    }
    }
`;

const LOGIN = gql`
  mutation($email: String, $password:String){
    login(email:$email, password:$password)
  }
`;

const ADD_ITEM = gql`
  mutation($params: ItemInput!) {
    addItem(input: $params){
      nama_item
      harga
      stock
    }
  }
`;

const ITEMS = gql`
{
	items{
    id_item
    nama_item
    harga
    stock
    status
  }
}

`;
export { USER, REGISTER, LOGIN, ADD_ITEM, ITEMS };