﻿import React, { useState } from "react";
import { Switch, Route, Link } from "react-router-dom";
import "./App.css";

import { ApolloClient, InMemoryCache, ApolloProvider } from '@apollo/client';

import NavBar from "./components/NavBar";

import Login from "./components/login";
import Register from "./components/register";
import User from "./components/User";
import Admin from "./components/Admin";

const client = new ApolloClient({
  uri: `http://localhost:3001/graphql`,
  cache: new InMemoryCache()
});

//TODO Web Template Studio: Add routes for your new pages here.
const App = () => {
  const [role,setRole] = useState("index");
    return (
      <ApolloProvider client={client}>
        <Switch>
          <Route exact path = "/" component = { () => <Login role={role} setRole={setRole}></Login> } />
          <Route path = "/login" component = { () => <Login role={role} setRole={setRole}></Login> } />
          <Route path = "/register" component = { Register } />
          <Route path = "/admin" component = { Admin } />
          <Route path = "/home" component = { User } />
        </Switch>
      </ApolloProvider>
    );
}

export default App;
