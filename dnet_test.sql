-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 22, 2020 at 03:59 AM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.4.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dnet_test`
--
CREATE DATABASE IF NOT EXISTS `dnet_test` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `dnet_test`;

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE `items` (
  `id_item` varchar(255) NOT NULL,
  `nama_item` varchar(255) NOT NULL,
  `harga` int(11) NOT NULL,
  `stock` int(11) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`id_item`, `nama_item`, `harga`, `stock`, `status`) VALUES
('plqgwfmkkcvn543i', 'paket internet 15k', 20000, 100, 1),
('plqgwjkskcwoni3k', 'qwe', 15000, 5, 1),
('plqgwjkskcwoszl8', 'item', 1, 1, 1),
('plqgwjkskcwp9wjx', 'item', 1, 1, 1),
('plqgwjkskcwpav0z', 'johananpoerwanto@gmail.com', 1, 1, 1),
('plqgwjkskcwpbskl', 'item', 1, 1, 1),
('plqgwjkskcwpbtth', 'item', 1, 1, 1),
('plqgwjkskcwpdiba', 'test', 333, 333, 1);

-- --------------------------------------------------------

--
-- Table structure for table `transaksi`
--

CREATE TABLE `transaksi` (
  `id_trans` varchar(255) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_item` varchar(255) NOT NULL,
  `total` int(11) NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `transaksi`
--

INSERT INTO `transaksi` (`id_trans`, `id_user`, `id_item`, `total`, `timestamp`) VALUES
('plqgwfmkkcvn6tf8', 1, 'plqgwfmkkcvn543i', 20000, '0000-00-00 00:00:00'),
('plqgw7u4kcvnn1cr', 1, 'plqgwfmkkcvn543i', 20000, '2020-07-21 00:00:00'),
('plqgw7vokcvp5na4', 2, 'plqgwfmkkcvn543i', 20000, '2020-07-21 15:49:35');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id_user` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `no_telp` varchar(15) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `saldo` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id_user`, `nama`, `email`, `password`, `no_telp`, `alamat`, `tgl_lahir`, `saldo`) VALUES
(2, 'qwe', 'test@gmail.com', '$2b$10$SUSX1.HKufQPOu7GdEq4Z.ZGOnGMdkE3iwSnzfqI5Ink9dAPOsLjS', '120390143', 'AA', '2020-10-01', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`id_item`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
