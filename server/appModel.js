const mysql = require("mysql");
const config = require("./config");
const pool = mysql.createPool(config.database);
const uniqid = require('uniqid');
const bcrypt = require('bcrypt');
const saltRounds = 10;
const salt = bcrypt.genSaltSync(saltRounds);

function getConnection() {
    return new Promise(function (resolve, reject) {
        pool.getConnection(function (err, conn) {
            if (err) reject(err);
            else resolve(conn);
        });
    });
}

function executeQuery(conn, query) {
    return new Promise(function (resolve, reject) {
        conn.query(query, function (err, res) {
            if (err) reject(err);
            else resolve(res);
        });
    });
}

async function login(email, password) {
    let conn = await getConnection();
    let getHash = await executeQuery(conn, `select * from users where email = '${email}'`);
    conn.release();
    return bcrypt.compareSync(password, getHash[0].password);
}

async function getUsers() {
    let conn = await getConnection();
    let query = `select * from users`;
    let result = await executeQuery(conn, query);
    conn.release();
    return result;
}

async function getUserData(id_user) {
    let conn = await getConnection();
    let query = `select * from users where id_user = '${id_user}'`;
    let result = await executeQuery(conn, query);
    conn.release();
    return result;
}

async function getItems() {
    let conn = await getConnection();
    let query = `select * from items`;
    let result = await executeQuery(conn, query);
    conn.release();
    return result;
}

async function getItemData(id_item) {
    let conn = await getConnection();
    let query = `select * from items where id_item = '${id_item}'`;
    let result = await executeQuery(conn, query);
    conn.release();
    return result;
}

async function getTrans(id_user) {
    let conn = await getConnection();
    let query = `select * from transaksi where id_user = '${id_user}'`;
    let result = await executeQuery(conn, query);
    conn.release();
    return result;
}

async function getAllTrans() {
    let conn = await getConnection();
    let query = `select * from transaksi`;
    let result = await executeQuery(conn, query);
    conn.release();
    return result;
}

async function register(nama, email, password, no_telp, alamat, tgl_lahir) {
    let conn = await getConnection();
    const hash = bcrypt.hashSync(password, salt);
    let query = `INSERT INTO users VALUES ('', '${nama}', '${email}','${hash}', '${no_telp}', '${alamat}', '${tgl_lahir}', '0')`;
    let result = await executeQuery(conn, query);
    conn.release();
    return result;
}

async function addTransaksi(id_user, id_item, total) {
    let conn = await getConnection();
    let query = `INSERT INTO transaksi VALUES('${uniqid()}', '${id_user}', '${id_item}', '${total}', now())`;
    let result = await executeQuery(conn, query);
    conn.release();
    return result;
}

async function addItem(nama_item, harga, stock) {
    let conn = await getConnection();
    let query = `INSERT INTO items VALUES('${uniqid()}', '${nama_item}', '${harga}', '${stock}', '1')`;
    let result = await executeQuery(conn, query);
    conn.release();
    return result;
}

async function updateItem(id_item, stock, status) {
    let conn = await getConnection();
    let query = `UPDATE items set stock = stock + ${stock}, status = '${status}' where id_item = '${id_item}'`;
    let result = await executeQuery(conn, query);
    conn.release();
    return result;
}

async function updateUser(id_user, nama, email, no_telp, alamat, tgl_lahir) {
    let conn = await getConnection();
    let query = `UPDATE users set 
    nama = '${nama}',
    email = '${email}',
    no_telp = '${no_telp}',
    alamat = '${alamat}',
    tgl_lahir = '${tgl_lahir}',
    where id_user = '${id_user}'`;
    let result = await executeQuery(conn, query);
    conn.release();
    return result;
}

async function topUp(id_user, saldo) {
    let conn = await getConnection();
    let query = `UPDATE users set 
    saldo = saldo + ${saldo},
    where id_user = '${id_user}'`;
    let result = await executeQuery(conn, query);
    conn.release();
    return result;
}

module.exports = {
    login,
    getUsers,
    getUserData,
    getItems,
    getItemData,
    getTrans,
    getAllTrans,
    register,
    addTransaksi,
    addItem,
    updateItem,
    updateUser,
    topUp,
}