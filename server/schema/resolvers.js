const appModel = require("../appModel");

const resolvers = {
    Query: {
        users: async () => await appModel.getUsers(),
        items: async () => await appModel.getItems(),
        trans: async (parent, {id_user}) => await appModel.getTrans(id_user),
        transaksi: async () => await appModel.getAllTrans(),
    },
    User: {
        transaksi: async (parent) => await appModel.getTrans(parent.id_user)
    },
    Transaksi: {
        user: async (parent) => await appModel.getUserData(parent.id_user),
        item: async (parent) => await appModel.getItemData(parent.id_item)
    },
    Mutation: {
        createUser: async (_, args) => {
            let data = args.input;
            if(
                data.nama != '' &&
                data.email != '' &&
                data.password != '' &&
                data.no_telp != '' &&
                data.alamat != '' &&
                data.tgl_lahir != '')
            {
                await appModel.register(data.nama, data.email, data.password, data.no_telp, data.alamat, data.tgl_lahir);
                return data;
            }
        },
        addItem: async (_, args) => {
            let data = args.input;
            await appModel.addItem(data.nama_item, data.harga, data.stock);
            return data;
        },
        addTrans: async (_, args) => {
            let data = args.input;
            await appModel.addTransaksi(data.id_user, data.id_item, data.total);
            return data;
        },
        login: async (parent, {email, password}) => {
            if(email === "admin" && password === "admin")
            {
                return "admin";
            }
            else
            {
                let check = await appModel.login(email, password);
                if(check)
                    return "user";
                else
                    return "false";
            }
        },
    }
}

module.exports = resolvers;