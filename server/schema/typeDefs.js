const { gql } = require("apollo-server-express");

const typeDefs = gql`
    type Query {
        hello: String!
        users: [User]
        items: [Item]
        trans(id_user:String!): [Transaksi]
        transaksi: [Transaksi]
    }

    type User {
        id_user:ID!
        nama:String!
        email:String!
        no_telp:String
        alamat:String
        tgl_lahir:String
        saldo:Int!
        status:Int!
        transaksi:[Transaksi]
    }

    type Item {
        id_item:String!
        nama_item:String!
        harga:Int!
        stock:Int!
        status:Int!
    }

    type Transaksi {
        id_trans:ID!
        total:Int
        timestamp:String
        id_user:String
        id_item:String
        user:[User]
        item:[Item]
    }

    type Mutation {
        createUser(input: UserInput!): User!
        addItem(input: ItemInput!): Item!
        addTrans(input: TransInput!): Transaksi!
        login(email: String, password: String): String!
    }

    input UserInput {
        nama:String!
        email:String!
        password:String!
        no_telp:String!
        alamat:String!
        tgl_lahir:String!
    }

    input ItemInput {
        nama_item:String!
        harga:Int!
        stock:Int!
    }

    input TransInput {
        id_user:ID!
        id_item:ID!
        total:Int
    }
`;

module.exports = typeDefs;