const config = {
    database: {
        host: "localhost",
        user: "root",
        password: "",
        port: 3306,
        database: "dnet_test",
    },

    server: {
        host: "127.0.0.1",
        port: 3001,
    },
};

module.exports = config;
