﻿const express = require("express");
const path = require("path");
const cookieParser = require("cookie-parser");	
const logger = require("morgan");
const { ApolloServer } = require('apollo-server-express');
const config = require("./config");
const resolvers = require('./schema/resolvers');
const typeDefs = require('./schema/typeDefs');

const app = express();
// app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(express.static(path.resolve(__dirname, "build")));

const server = new ApolloServer({ typeDefs, resolvers });
server.applyMiddleware({ app });

// TODO Web Template Studio: Add your own error handler here.
if (process.env.NODE_ENV === "production") {
  // Do not send stack trace of error message when in production
  app.use((err, req, res, next) => {
    res.status(err.status || 500);
    res.send("Error occurred while handling the request.");
  });
} else {
  // Log stack trace of error message while in development
  app.use((err, req, res, next) => {
    res.status(err.status || 500);
    console.log(err);
    res.send(err.message);
  });
}

app.listen({ port: config.server.port }, () =>
    console.log(`🚀 Server ready at http://localhost:${config.server.port}${server.graphqlPath}`)
)
